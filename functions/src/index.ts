import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as Firestore from "@google-cloud/firestore";

admin.initializeApp();

const REGION = "europe-west3";
const RUNTIME_OPTIONS: functions.RuntimeOptions = {
	memory: "128MB",
	timeoutSeconds: 60,
};
const USERS = "users";
const FCM_TOKEN = "fcmToken";
const SHOPPING_LISTS = "shoppingLists";
const ITEMS = "items";
const MEMBERS = "members";
const PENDING_JOIN_REQUESTS = "pendingJoinRequests";
const NAME = "name";
const QUANTITY = "quantity";
const UNIT_OF_MEASURE = "unitOfMeasure";

export const joinToShoppingList = functions
	.region(REGION)
	.runWith(RUNTIME_OPTIONS)
	.firestore.document(`/${PENDING_JOIN_REQUESTS}/{requestId}`)
	.onCreate((snapshot, context) => {
		const data = snapshot.data();
		if (data) {
			const userUid = data.userUid;
			const shoppingListId = data.shoppingListId;
			let p1 = snapshot.ref.delete();
			let p2 = admin
				.firestore()
				.doc(`/${SHOPPING_LISTS}/${shoppingListId}`)
				.update(`${MEMBERS}`, Firestore.FieldValue.arrayUnion(userUid));
			return Promise.all([p1, p2]);
		} else {
			return null;
		}
	});

export const sendNotification = functions
	.region(REGION)
	.runWith(RUNTIME_OPTIONS)
	.firestore.document(`/${SHOPPING_LISTS}/{shoppingListId}/${ITEMS}/{itemId}`)
	.onWrite(async (change, context) => {
		const title =
			change.after.get(`${NAME}`) +
			": " +
			change.after.get(`${QUANTITY}`) +
			" " +
			change.after.get(`${UNIT_OF_MEASURE}`);
		const body = "Tap to see it in the app!";
		const shoppingList = await change.after.ref.parent.parent?.get();
		const members: string[] = shoppingList?.get(`${MEMBERS}`);
		const promises: Promise<any>[] = [];
		members.forEach((uid: string) => {
			const promise = admin
				.firestore()
				.doc(`/${USERS}/${uid}`)
				.get()
				.then((snapshot) => {
					const fcmToken = snapshot.get(`${FCM_TOKEN}`);
					const message: admin.messaging.Message = {
						notification: {
							title: title,
							body: body,
						},
						token: fcmToken,
					};
					return admin.messaging().send(message);
				})
				.catch((error) => {
					console.log(error);
				});
			promises.push(promise);
		});
		return Promise.all(promises);
	});

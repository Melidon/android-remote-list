package hu.bme.aut.android.remotelist

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.remotelist.adapter.ItemsAdapter
import hu.bme.aut.android.remotelist.data.Item
import hu.bme.aut.android.remotelist.data.ShoppingList
import hu.bme.aut.android.remotelist.databinding.ActivityItemsBinding
import hu.bme.aut.android.remotelist.db.DataBase
import hu.bme.aut.android.remotelist.dialog.ItemDialog

class ItemsActivity : AppCompatActivity(), ItemDialog.ItemHandler {

    companion object {
        private const val TAG = "ItemsActivity"

        fun createIntent(context: Context, id: String): Intent {
            return Intent().setClass(context, ItemsActivity::class.java).putExtra("LIST_ID", id)
        }
    }

    private lateinit var mBinding: ActivityItemsBinding
    private lateinit var itemsAdapter: ItemsAdapter
    private lateinit var shoppingListId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(AuthUiActivity.createIntent(this))
            finish()
            return
        }


        shoppingListId = intent.getStringExtra("LIST_ID")!!

        mBinding = ActivityItemsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.topAppBar)

        itemsAdapter = ItemsAdapter(this)
        mBinding.rvShoppingLists.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        mBinding.rvShoppingLists.adapter = itemsAdapter

        mBinding.topAppBar.setNavigationOnClickListener {
            finish()
        }

        mBinding.floatingActionButton.setOnClickListener {
            createItemClicked()
        }

        initShoppingListListener()
        initItemsListener()
    }

    private fun initShoppingListListener() {
        DataBase.getInstance().addShoppingListsListener { snapshots, error ->
            if (error != null) {
                Log.w(TAG, "Error at snapshot listener", error)
                return@addShoppingListsListener
            }
            for (dc in snapshots!!.documentChanges) {
                if (dc.document.id == shoppingListId) {
                    when (dc.type) {
                        DocumentChange.Type.ADDED -> {
                            mBinding.topAppBar.title = dc.document.toObject<ShoppingList>().name
                        }
                        DocumentChange.Type.REMOVED -> {
                            Toast.makeText(
                                this,
                                getString(R.string.list_deleted_by_someone_else),
                                Toast.LENGTH_LONG
                            ).show()
                            finish()
                            return@addShoppingListsListener
                        }
                        else -> {
                        }
                    }
                }
            }
        }
    }

    private fun initItemsListener() {
        DataBase.getInstance().addItemsListener(shoppingListId) { snapshots, error ->
            if (error != null) {
                Log.w(TAG, "Error at snapshot listener", error)
                return@addItemsListener
            }
            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        mBinding.hint.isVisible = false
                        itemsAdapter.addItem(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        itemsAdapter.modifyItem(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        itemsAdapter.removeItem(dc.document.id)
                        if (itemsAdapter.itemCount == 0) {
                            mBinding.hint.isVisible = true
                        }
                    }
                }
            }
        }
    }

    private fun createItemClicked() {
        ItemDialog().show(supportFragmentManager, "NEW_ITEM")
    }

    override fun createItem(itemToCreate: Item) {
        DataBase.getInstance().createItem(shoppingListId, itemToCreate)
    }

    fun editItemClicked(itemId: String, itemToEdit: Item) {
        val editDialog = ItemDialog()
        val bundle = Bundle()
        bundle.putString(ItemDialog.ITEM_ID, itemId)
        bundle.putSerializable(ItemDialog.ITEM_TO_EDIT, itemToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, "EDIT_ITEM")
    }

    override fun updateItem(itemId: String, itemToUpdate: Item) {
        DataBase.getInstance().updateItem(shoppingListId, itemId, itemToUpdate)
    }

    fun deleteItemClicked(itemId: String) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.are_you_sure_you_want_to_delete_this_item_for_everyone))
            .setPositiveButton(getString(R.string.yes_delete_it)) { _, _ -> deleteItem(itemId) }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun deleteItem(itemId: String) {
        DataBase.getInstance().deleteItem(shoppingListId, itemId)
    }
}
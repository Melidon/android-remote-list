package hu.bme.aut.android.remotelist

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.InflateException
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import com.firebase.ui.auth.util.ExtraConstants
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.ktx.toObject
import hu.bme.aut.android.remotelist.adapter.ShoppingListsAdapter
import hu.bme.aut.android.remotelist.data.ShoppingList
import hu.bme.aut.android.remotelist.databinding.ActivityShoppingListsBinding
import hu.bme.aut.android.remotelist.db.DataBase
import hu.bme.aut.android.remotelist.dialog.JoinDialog
import hu.bme.aut.android.remotelist.dialog.ShoppingListDialog

class ShoppingListsActivity : AppCompatActivity(), ShoppingListDialog.ShoppingListHandler,
    JoinDialog.JoinHandler {

    companion object {
        private const val TAG = "ShoppingListsActivity"

        fun createIntent(context: Context, response: IdpResponse?): Intent {
            return Intent().setClass(context, ShoppingListsActivity::class.java)
                .putExtra(ExtraConstants.IDP_RESPONSE, response)
        }
    }

    private lateinit var mBinding: ActivityShoppingListsBinding
    private lateinit var shoppingListsAdapter: ShoppingListsAdapter
    private lateinit var user: FirebaseUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(AuthUiActivity.createIntent(this))
            finish()
            return
        }

        user = currentUser

        mBinding = ActivityShoppingListsBinding.inflate(layoutInflater)
        setContentView(mBinding.root)
        setSupportActionBar(mBinding.topAppBar)

        shoppingListsAdapter = ShoppingListsAdapter(this)
        mBinding.rvShoppingLists.layoutManager = LinearLayoutManager(this).apply {
            reverseLayout = true
            stackFromEnd = true
        }
        mBinding.rvShoppingLists.adapter = shoppingListsAdapter

        mBinding.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId) {
                R.id.sign_out -> {
                    signOut()
                    true
                }
                R.id.delete_account -> {
                    deleteAccountClicked()
                    true
                }
                else -> false
            }
        }

        mBinding.newList.setOnClickListener {
            createShoppingListClicked()
        }
        mBinding.joinWithACode.setOnClickListener {
            joinClicked()
        }

        initShoppingListsListener()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val ancestor = super.onCreateOptionsMenu(menu)
        var exception = false
        try {
            menuInflater.inflate(R.menu.shopping_lists_app_bar, menu)
        } catch (ie: InflateException) {
            exception = true
        }
        return ancestor && !exception
    }

    private fun signOut() {
        AuthUI.getInstance()
            .signOut(this)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this))
                    finish()
                } else {
                    Log.w(TAG, "signOut:failure", task.exception)
                    showSnackbar(R.string.sign_out_failed)
                }
            }
    }

    private fun deleteAccountClicked() {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.are_you_sure_you_want_to_delete_this_account))
            .setPositiveButton(getString(R.string.yes_delete_it)) { _, _ -> deleteAccount() }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun deleteAccount() {
        AuthUI.getInstance()
            .delete(this)
            .addOnCompleteListener(
                this
            ) { task ->
                if (task.isSuccessful) {
                    startActivity(AuthUiActivity.createIntent(this))
                    finish()
                } else {
                    showSnackbar(R.string.delete_account_failed)
                }
            }
    }

    fun startItemsActivity(shoppingListId: String) {
        startActivity(ItemsActivity.createIntent(this, shoppingListId))
    }

    private fun joinClicked() {
        JoinDialog().show(supportFragmentManager, "null")
    }

    override fun join(code: String) {
        DataBase.getInstance().joinShoppinglist(code)
    }

    private fun initShoppingListsListener() {
        DataBase.getInstance().addShoppingListsListener { snapshots, error ->
            if (error != null) {
                Log.w(TAG, "Error at snapshot listener", error)
                return@addShoppingListsListener
            }

            for (dc in snapshots!!.documentChanges) {
                when (dc.type) {
                    DocumentChange.Type.ADDED -> {
                        mBinding.hint.isVisible = false
                        shoppingListsAdapter.addShoppingList(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.MODIFIED -> {
                        shoppingListsAdapter.modifyShoppingList(
                            dc.document.toObject(),
                            dc.document.id
                        )
                    }
                    DocumentChange.Type.REMOVED -> {
                        shoppingListsAdapter.removeShoppingList(dc.document.id)
                        if (shoppingListsAdapter.itemCount == 0) {
                            mBinding.hint.isVisible = true
                        }
                    }
                }
            }
        }
    }

    fun shareShoppingListClicked(shoppingListId: String) {
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.putExtra(Intent.EXTRA_TEXT, shoppingListId)
        shareIntent.type = "text/plain"
        startActivity(Intent.createChooser(shareIntent, "Share shopping list"))
    }

    private fun createShoppingListClicked() {
        ShoppingListDialog().show(supportFragmentManager, "NEW_SHOPPING_LIST")
    }

    override fun createShoppingList(name: String) {
        DataBase.getInstance().createShoppingList(ShoppingList(name, mutableListOf(user.uid)))
    }

    fun editShoppingListClicked(shoppingListId: String, shoppingListToEdit: ShoppingList) {
        val editDialog = ShoppingListDialog()
        val bundle = Bundle()
        bundle.putString(ShoppingListDialog.SHOPPING_LIST_ID, shoppingListId)
        bundle.putSerializable(ShoppingListDialog.SHOPPING_LIST_TO_EDIT, shoppingListToEdit)
        editDialog.arguments = bundle
        editDialog.show(supportFragmentManager, "EDIT_SHOPPING_LIST")
    }

    override fun updateShoppingList(shoppingListId: String, shoppingListToUpdate: ShoppingList) {
        DataBase.getInstance().updateShoppingList(shoppingListId, shoppingListToUpdate)
    }

    fun deleteShoppingListClicked(shoppingListId: String) {
        AlertDialog.Builder(this)
            .setMessage(getString(R.string.are_you_sure_you_want_to_delete_this_shopping_list_for_everyone))
            .setPositiveButton(getString(R.string.yes_delete_it)) { _, _ ->
                deleteShoppingList(shoppingListId)
            }
            .setNegativeButton(getString(R.string.no), null)
            .show()
    }

    private fun deleteShoppingList(shoppingListId: String) {
        DataBase.getInstance().deleteShoppingList(shoppingListId)
    }

    private fun showSnackbar(errorMessageRes: Int) {
        Snackbar.make(mBinding.root, getString(errorMessageRes), Snackbar.LENGTH_LONG).show()
    }

}

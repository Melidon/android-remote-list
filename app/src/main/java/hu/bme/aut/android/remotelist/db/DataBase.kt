package hu.bme.aut.android.remotelist.db

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import hu.bme.aut.android.remotelist.data.Item
import hu.bme.aut.android.remotelist.data.ShoppingList

class DataBase {

    companion object {
        private const val TAG = "DataBase"

        private const val USERS = "users"
        private const val FCM_TOKEN = "fcmToken"
        private const val SHOPPING_LISTS = "shoppingLists"
        private const val ITEMS = "items"
        private const val MEMBERS = "members"
        private const val USER_UID = "userUid"
        private const val SHOPPING_LIST_ID = "shoppingListId"
        private const val PENDING_JOIN_REQUESTS = "pendingJoinRequests"

        private var instance = DataBase()
        fun getInstance(): DataBase {
            return instance
        }
    }

    private var db = Firebase.firestore

    fun sendRegistrationToServer(token: String) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        db.collection(USERS).document(user.uid).set(hashMapOf(FCM_TOKEN to token))
    }

    fun joinShoppinglist(shoppingListId: String) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        val joinRequest = hashMapOf<String, Any>(
            USER_UID to user.uid,
            SHOPPING_LIST_ID to shoppingListId
        )
        db.collection(PENDING_JOIN_REQUESTS)
            .add(joinRequest)
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "Join request added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error adding join request", error)
            }
    }

    fun createShoppingList(shoppingListToCreate: ShoppingList) {
        db.collection(SHOPPING_LISTS)
            .add(shoppingListToCreate.toHashMap())
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "Shopping list added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error adding shopping list", error)
            }
    }

    fun addShoppingListsListener(listener: EventListener<QuerySnapshot?>) {
        val user = FirebaseAuth.getInstance().currentUser
        user ?: return
        db.collection(SHOPPING_LISTS).whereArrayContains(MEMBERS, user.uid)
            .addSnapshotListener(listener)
    }

    fun updateShoppingList(shoppingListId: String, shoppingListToUpdate: ShoppingList) {
        db.collection(SHOPPING_LISTS).document(shoppingListId)
            .update(shoppingListToUpdate.toHashMap())
    }

    fun deleteShoppingList(shoppingListId: String) {
        db.collection(SHOPPING_LISTS).document(shoppingListId).delete()
            .addOnSuccessListener {
                Log.d(TAG, "Shopping list deleted with ID: $shoppingListId")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error deleting shopping list", error)
            }
    }

    fun createItem(shoppingListId: String, itemToCreate: Item) {
        db.collection(SHOPPING_LISTS).document(shoppingListId).collection("items")
            .add(itemToCreate.toHashMap())
            .addOnSuccessListener { documentReference ->
                Log.d(TAG, "Item added with ID: ${documentReference.id}")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error adding item", error)
            }
    }

    fun addItemsListener(shoppingListId: String, listener: EventListener<QuerySnapshot?>) {
        db.collection(SHOPPING_LISTS).document(shoppingListId).collection(ITEMS)
            .addSnapshotListener(listener)
    }

    fun updateItem(shoppingListId: String, itemId: String, itemToUpdate: Item) {
        db.collection(SHOPPING_LISTS).document(shoppingListId).collection(ITEMS)
            .document(itemId)
            .update(itemToUpdate.toHashMap())
    }

    fun deleteItem(shoppingListId: String, itemId: String) {
        db.collection(SHOPPING_LISTS).document(shoppingListId).collection(ITEMS)
            .document(itemId)
            .delete()
            .addOnSuccessListener {
                Log.d(TAG, "Item deleted with ID: $itemId")
            }
            .addOnFailureListener { error ->
                Log.w(TAG, "Error deleting item", error)
            }
    }
}
package hu.bme.aut.android.remotelist.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.remotelist.R


class JoinDialog : DialogFragment() {

    interface JoinHandler {
        fun join(code: String)
    }

    private lateinit var joinHandler: JoinHandler

    private lateinit var etCode: EditText

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is JoinHandler) {
            joinHandler = context
        } else {
            throw RuntimeException("The Activity does not implement the JoinHandler interface")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val rootView = inflater.inflate(R.layout.dialog_join, null)
            builder
                .setTitle(R.string.join_with_a_code)
                .setView(rootView)
                .setPositiveButton(R.string.join, null)
                .setNegativeButton(R.string.cancel) { _, _ -> dialog?.cancel() }
            etCode = rootView.findViewById(R.id.etCode)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validate()) {
                val code = etCode.text.toString()
                joinHandler.join(code)
                dialog.dismiss()
            }
        }
    }

    companion object {
        private const val VALID_CODE_LENGTH = 20
    }

    private fun validate(): Boolean {
        if (etCode.text.isEmpty()) {
            etCode.error = getString(R.string.code_can_not_be_empty)
            return false
        }
        if (etCode.text.length != VALID_CODE_LENGTH) {
            etCode.error = getString(R.string.the_length_of_a_valid_code_is_) + VALID_CODE_LENGTH
            return false
        }
        return true
    }

}

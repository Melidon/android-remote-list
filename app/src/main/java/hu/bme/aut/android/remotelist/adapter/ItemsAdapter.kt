package hu.bme.aut.android.remotelist.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.remotelist.ItemsActivity
import hu.bme.aut.android.remotelist.data.Item
import hu.bme.aut.android.remotelist.databinding.CardItemBinding

class ItemsAdapter(private val context: Context) :
    ListAdapter<Item, ItemsAdapter.ItemViewHolder>(ItemCallback) {

    private val itemList: MutableList<Item> = mutableListOf()
    private val itemIdMap: MutableMap<Item, String> = mutableMapOf()
    private var lastPosition = -1

    class ItemViewHolder(binding: CardItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val mBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ItemViewHolder(
            CardItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val tmpItem = itemList[position]
        holder.mBinding.tvName.text = tmpItem.name
        holder.mBinding.tvQuantity.text = tmpItem.quantity.toString()
        holder.mBinding.tvUnitOfMeasure.text = tmpItem.unitOfMeasure
        setAnimation(holder.itemView, position)
        holder.mBinding.btnDelete.setOnClickListener {
            (context as ItemsActivity).deleteItemClicked(itemIdMap[tmpItem]!!)
        }
        holder.mBinding.btnEdit.setOnClickListener {
            (context as ItemsActivity).editItemClicked(itemIdMap[tmpItem]!!, tmpItem)
        }
    }

    fun addItem(newItem: Item?, id: String) {
        newItem ?: return
        itemIdMap[newItem] = id
        itemList += newItem
        submitList(itemList)
        notifyItemInserted(itemList.lastIndex)
        Log.d(TAG, "addItem:called")
    }

    fun modifyItem(modifiedItem: Item?, id: String) {
        modifiedItem ?: return
        val index = itemList.indexOfFirst { item: Item ->
            itemIdMap[item].equals(id)
        }
        itemIdMap.remove(itemList[index])
        itemList[index] = modifiedItem
        itemIdMap[itemList[index]] = id
        submitList(itemList)
        notifyItemChanged(index)
        Log.d(TAG, "modifyItem:called")
    }

    fun removeItem(id: String) {
        val index = itemList.indexOfFirst { item: Item ->
            itemIdMap[item].equals(id)
        }
        itemIdMap.remove(itemList[index])
        itemList.removeAt(index)
        submitList(itemList)
        notifyItemRemoved(index)
        Log.d(TAG, "removeItem:called")
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {

        private const val TAG = "ItemsAdapter"

        object ItemCallback : DiffUtil.ItemCallback<Item>() {
            override fun areItemsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Item, newItem: Item): Boolean {
                return oldItem == newItem
            }
        }
    }
}

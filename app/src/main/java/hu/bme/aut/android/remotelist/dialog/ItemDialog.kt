package hu.bme.aut.android.remotelist.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.remotelist.R
import hu.bme.aut.android.remotelist.data.Item
import kotlin.properties.Delegates


class ItemDialog : DialogFragment() {

    interface ItemHandler {
        fun createItem(itemToCreate: Item)
        fun updateItem(itemId: String, itemToUpdate: Item)
    }

    companion object {
        const val ITEM_ID = "ITEM_ID"
        const val ITEM_TO_EDIT = "ITEM_TO_EDIT"
    }

    private var isEditDialog by Delegates.notNull<Boolean>()
    private lateinit var itemHandler: ItemHandler

    private lateinit var etName: EditText
    private lateinit var etQuantity: EditText
    private lateinit var etUnitOfMeasure: EditText

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ItemHandler) {
            itemHandler = context
        } else {
            throw RuntimeException("The Activity does not implement the ItemHandler interface")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isEditDialog = (arguments != null && arguments!!.containsKey(ITEM_TO_EDIT))
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val rootView = inflater.inflate(R.layout.dialog_create_item, null)
            builder
                .setTitle(
                    when (isEditDialog) {
                        true -> R.string.edit_item
                        false -> R.string.new_item
                    }
                )
                .setView(rootView)
                .setPositiveButton(
                    when (isEditDialog) {
                        true -> R.string.edit
                        false -> R.string.create
                    }, null
                )
                .setNegativeButton(R.string.cancel) { _, _ -> dialog?.cancel() }
            etName = rootView.findViewById(R.id.etName)
            etQuantity = rootView.findViewById(R.id.etQuantity)
            etUnitOfMeasure = rootView.findViewById(R.id.etUnitOfMeasure)
            if (isEditDialog) {
                val itemToEdit = arguments!!.getSerializable(ITEM_TO_EDIT) as Item
                etName.setText(itemToEdit.name)
                etQuantity.setText(itemToEdit.quantity.toString())
                etUnitOfMeasure.setText(itemToEdit.unitOfMeasure)
            }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validate()) {
                when (isEditDialog) {
                    true -> handleItemEdit()
                    false -> handleItemCreate()
                }
                dialog.dismiss()
            }
        }
    }

    private fun validate(): Boolean {
        val validName = etName.text.isNotEmpty()
        if (!validName) {
            etName.error = getString(R.string.name_can_not_be_empty)
        }
        val validQuantity = etQuantity.text.isNotEmpty()
        if (!validQuantity) {
            etQuantity.error = getString(R.string.quantity_can_not_be_empty)
        }
        val validUnitOfMeasure = true
        if (!validUnitOfMeasure) {
            // It is always valid
        }
        return validName && validQuantity && validUnitOfMeasure
    }

    private fun handleItemCreate() {
        itemHandler.createItem(
            Item(
                etName.text.toString(),
                etQuantity.text.toString().toDouble(),
                etUnitOfMeasure.text.toString()
            )
        )
    }

    private fun handleItemEdit() {
        val itemToEdit = arguments!!.getSerializable(ITEM_TO_EDIT) as Item
        itemToEdit.name = etName.text.toString()
        itemToEdit.quantity = etQuantity.text.toString().toDouble()
        itemToEdit.unitOfMeasure = etUnitOfMeasure.text.toString()
        val itemId = arguments!!.getString(ITEM_ID)!!
        itemHandler.updateItem(itemId, itemToEdit)
    }
}

package hu.bme.aut.android.remotelist.data

import java.io.Serializable

class Item(
    var name: String? = null,
    var quantity: Double? = null,
    var unitOfMeasure: String? = null
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "name" to name,
            "quantity" to quantity,
            "unitOfMeasure" to unitOfMeasure
        )
    }
}
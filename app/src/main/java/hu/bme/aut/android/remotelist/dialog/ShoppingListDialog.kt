package hu.bme.aut.android.remotelist.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import hu.bme.aut.android.remotelist.R
import hu.bme.aut.android.remotelist.data.ShoppingList
import kotlin.properties.Delegates


class ShoppingListDialog : DialogFragment() {

    interface ShoppingListHandler {
        fun createShoppingList(name: String)
        fun updateShoppingList(shoppingListId: String, shoppingListToUpdate: ShoppingList)
    }

    companion object {
        const val SHOPPING_LIST_ID = "SHOPPING_LIST_ID"
        const val SHOPPING_LIST_TO_EDIT = "SHOPPING_LIST_TO_EDIT"
    }

    private var isEditDialog by Delegates.notNull<Boolean>()
    private lateinit var shoppingListHandler: ShoppingListHandler

    private lateinit var etName: EditText

    override fun onAttach(context: Context) {
        super.onAttach(context)

        if (context is ShoppingListHandler) {
            shoppingListHandler = context
        } else {
            throw RuntimeException("The Activity does not implement the ShoppingListHandler interface")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        isEditDialog = (arguments != null && arguments!!.containsKey(SHOPPING_LIST_TO_EDIT))
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater
            val rootView = inflater.inflate(R.layout.dialog_create_shopping_list, null)
            builder
                .setTitle(
                    when (isEditDialog) {
                        true -> R.string.edit_list
                        false -> R.string.new_list
                    }
                )
                .setView(rootView)
                .setPositiveButton(
                    when (isEditDialog) {
                        true -> R.string.edit
                        false -> R.string.create
                    }, null
                )
                .setNegativeButton(R.string.cancel) { _, _ -> dialog?.cancel() }
            etName = rootView.findViewById(R.id.etName)
            if (isEditDialog) {
                val shoppingListToEdit =
                    arguments!!.getSerializable(SHOPPING_LIST_TO_EDIT) as ShoppingList
                etName.setText(shoppingListToEdit.name)
            }
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onResume() {
        super.onResume()

        val dialog = dialog as AlertDialog
        val positiveButton = dialog.getButton(Dialog.BUTTON_POSITIVE)

        positiveButton.setOnClickListener {
            if (validate()) {
                when (isEditDialog) {
                    true -> handleShoppingListEdit()
                    false -> handleShoppingListCreate()
                }
                dialog.dismiss()
            }
        }
    }

    private fun validate(): Boolean {
        val validName = etName.text.isNotEmpty()
        if (!validName) {
            etName.error = getString(R.string.name_can_not_be_empty)
        }
        return validName
    }

    private fun handleShoppingListCreate() {
        shoppingListHandler.createShoppingList(etName.text.toString())
    }

    private fun handleShoppingListEdit() {
        val shoppingListToEdit = arguments!!.getSerializable(SHOPPING_LIST_TO_EDIT) as ShoppingList
        shoppingListToEdit.name = etName.text.toString()
        val shoppingListId = arguments!!.getString(SHOPPING_LIST_ID)!!
        shoppingListHandler.updateShoppingList(shoppingListId, shoppingListToEdit)
    }
}

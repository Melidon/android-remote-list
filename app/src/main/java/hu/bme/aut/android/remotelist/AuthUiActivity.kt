package hu.bme.aut.android.remotelist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import hu.bme.aut.android.remotelist.databinding.ActivityAuthUiBinding
import hu.bme.aut.android.remotelist.db.DataBase

class AuthUiActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "AuthUiActivity"
        private const val RC_SIGN_IN = 100

        fun createIntent(context: Context): Intent {
            return Intent(context, AuthUiActivity::class.java)
        }
    }

    private lateinit var mBinding: ActivityAuthUiBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityAuthUiBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        mBinding.signIn.setOnClickListener {
            signIn()
        }

    }

    private fun signIn() {
        startActivityForResult(buildSignInIntent(), RC_SIGN_IN)
    }

    private fun buildSignInIntent(): Intent {
        val builder = AuthUI.getInstance().createSignInIntentBuilder()
            .setTheme(getAppTheme())
            .setLogo(getAppLogo())
            .setAvailableProviders(getProviders())
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null && auth.currentUser!!.isAnonymous) {
            builder.enableAnonymousUsersAutoUpgrade()
        }
        return builder.build()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            handleSignInResponse(resultCode, data)
        }
    }

    override fun onResume() {
        super.onResume()
        val auth = FirebaseAuth.getInstance()
        if (auth.currentUser != null && intent.extras == null) {
            startSignedInActivity(null)
            finish()
        }
    }

    private fun handleSignInResponse(resultCode: Int, data: Intent?) {
        val response = IdpResponse.fromResultIntent(data)

        // Successfully signed in
        if (resultCode == RESULT_OK) {
            registerToServer()
            startSignedInActivity(response)
            finish()
        } else {
            // Sign in failed
            if (response == null) {
                // User pressed back button
                showSnackbar(R.string.sign_in_cancelled)
                return
            }
            if (response.error!!.errorCode == ErrorCodes.NO_NETWORK) {
                showSnackbar(R.string.no_internet_connection)
                return
            }
            if (response.error!!.errorCode == ErrorCodes.ERROR_USER_DISABLED) {
                showSnackbar(R.string.account_disabled)
                return
            }
            showSnackbar(R.string.unknown_error)
            Log.e(TAG, "Sign-in error: ", response.error)
        }
    }

    private fun registerToServer() {
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(TAG, "Fetching registerToServer registration token failed", task.exception)
                return@addOnCompleteListener
            }
            val token = task.result!!
            DataBase.getInstance().sendRegistrationToServer(token)
        }
    }

    private fun startSignedInActivity(response: IdpResponse?) {
        startActivity(ShoppingListsActivity.createIntent(this, response))
    }

    private fun getAppTheme(): Int {
        return R.style.Theme_RemoteList_Auth
    }

    private fun getAppLogo(): Int {
        return R.drawable.cart_128dp
    }

    private fun getProviders(): ArrayList<AuthUI.IdpConfig> {
        return arrayListOf(
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.EmailBuilder().build()
        )
    }

    private fun showSnackbar(errorMessageRes: Int) {
        Snackbar.make(mBinding.root, getString(errorMessageRes), Snackbar.LENGTH_LONG).show()
    }

}
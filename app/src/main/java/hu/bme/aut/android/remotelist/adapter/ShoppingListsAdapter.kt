package hu.bme.aut.android.remotelist.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.bme.aut.android.remotelist.ShoppingListsActivity
import hu.bme.aut.android.remotelist.data.ShoppingList
import hu.bme.aut.android.remotelist.databinding.CardShoppingListBinding

class ShoppingListsAdapter(private val context: Context) :
    ListAdapter<ShoppingList, ShoppingListsAdapter.ShoppingListViewHolder>(ItemCallback) {

    private val shoppingListList: MutableList<ShoppingList> = mutableListOf()
    private val shoppingListIdMap: MutableMap<ShoppingList, String> = mutableMapOf()
    private var lastPosition = -1

    class ShoppingListViewHolder(binding: CardShoppingListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val tvName: TextView = binding.tvName
        val mBinding = binding
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ShoppingListViewHolder(
            CardShoppingListBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: ShoppingListViewHolder, position: Int) {
        val tmpShoppingList = shoppingListList[position]
        val tmpShoppingListId = shoppingListIdMap[tmpShoppingList]!!
        holder.tvName.text = tmpShoppingList.name
        setAnimation(holder.itemView, position)
        holder.mBinding.root.setOnClickListener {
            (context as ShoppingListsActivity).startItemsActivity(tmpShoppingListId)
        }
        holder.mBinding.btnDelete.setOnClickListener {
            (context as ShoppingListsActivity).deleteShoppingListClicked(tmpShoppingListId)
        }
        holder.mBinding.btnEdit.setOnClickListener {
            (context as ShoppingListsActivity).editShoppingListClicked(
                tmpShoppingListId,
                tmpShoppingList
            )
        }
        holder.mBinding.btnShare.setOnClickListener {
            (context as ShoppingListsActivity).shareShoppingListClicked(tmpShoppingListId)
        }
    }

    fun addShoppingList(newShoppingList: ShoppingList?, shoppingListId: String) {
        newShoppingList ?: return
        shoppingListIdMap[newShoppingList] = shoppingListId
        shoppingListList += newShoppingList
        submitList(shoppingListList)
        notifyItemInserted(shoppingListList.lastIndex)
        Log.d(TAG, "addShoppingList:called")
    }

    fun modifyShoppingList(modifiedShoppingList: ShoppingList?, shoppingListId: String) {
        modifiedShoppingList ?: return
        val index = shoppingListList.indexOfFirst { shoppingList: ShoppingList ->
            shoppingListIdMap[shoppingList].equals(shoppingListId)
        }
        shoppingListIdMap.remove(shoppingListList[index])
        shoppingListList[index] = modifiedShoppingList
        shoppingListIdMap[shoppingListList[index]] = shoppingListId
        submitList(shoppingListList)
        notifyItemChanged(index)
        Log.d(TAG, "modifyShoppingList:called")
    }

    fun removeShoppingList(shoppingListId: String) {
        val index = shoppingListList.indexOfFirst { shoppingList: ShoppingList ->
            shoppingListIdMap[shoppingList].equals(shoppingListId)
        }
        shoppingListIdMap.remove(shoppingListList[index])
        shoppingListList.removeAt(index)
        submitList(shoppingListList)
        notifyItemRemoved(index)
        Log.d(TAG, "removeShoppingList:called")
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    companion object {

        private const val TAG = "ShoppingListsAdapter"

        object ItemCallback : DiffUtil.ItemCallback<ShoppingList>() {
            override fun areItemsTheSame(
                oldShoppingList: ShoppingList,
                newShoppingList: ShoppingList
            ): Boolean {
                return oldShoppingList == newShoppingList
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(
                oldShoppingList: ShoppingList,
                newShoppingList: ShoppingList
            ): Boolean {
                return oldShoppingList == newShoppingList
            }
        }
    }
}

package hu.bme.aut.android.remotelist.data

import java.io.Serializable

class ShoppingList(
    var name: String? = null,
    var members: List<String>? = null
) : Serializable {
    fun toHashMap(): HashMap<String, Any?> {
        return hashMapOf(
            "name" to name,
            "members" to members
        )
    }
}

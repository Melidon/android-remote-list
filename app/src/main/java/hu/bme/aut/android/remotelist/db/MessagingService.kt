package hu.bme.aut.android.remotelist.db

import com.google.firebase.messaging.FirebaseMessagingService

class MessagingService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        DataBase.getInstance().sendRegistrationToServer(token)
    }
}